package com.example.intellijidea_samplepj;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IntellijIdeaSamplePjApplication {

    public static void main(String[] args) {
        SpringApplication.run(IntellijIdeaSamplePjApplication.class, args);
    }

}
