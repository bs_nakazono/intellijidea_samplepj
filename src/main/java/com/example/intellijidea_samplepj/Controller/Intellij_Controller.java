package com.example.intellijidea_samplepj.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class Intellij_Controller {

    private static final String VIEW = "Sample";

    @GetMapping("/hello")
        public ModelAndView GetSmaple(ModelAndView mav){

        mav.setViewName(VIEW);
        return mav;
    }

}
