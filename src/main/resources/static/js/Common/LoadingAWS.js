

//画面ローディング(AWS風) Nakazono
window.onbeforeunload = function(e) {

    //画面の高さを取得
    var h = $(window).height();
    //コンテンツを非表示に
    $('#contents').css('display','none');
    //ローディング画像を表示
    $('#loader-bg ,#loader').height(h).css('display','block');
}


//画面ロードしたら実行される。
window.onload = function () {

    // //確認ボタンを非表示にする
    // $('#BtConfirm').fadeOut();

    //ローディングを非表示にする
    $('#loader-bg').delay(900).fadeOut(800);
    $('#loader').delay(800).fadeOut(800);
    //コンテンツを表示する
    $('#contents').css('display', 'block');

}

//8秒で強制的にローディング終了
$(function(){
    setTimeout('stopload()',8000);
});

function stopload(){//強制表示
    $('#contents').css('display','block');
    $('#loader-bg').delay(900).fadeOut(800);
    $('#loader').delay(800).fadeOut(800);
}


